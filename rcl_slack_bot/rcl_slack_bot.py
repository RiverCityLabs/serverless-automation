import os
from flask import abort, Flask, jsonify, request

from shared_scripts.login_handler import LoginHandler
from shared_scripts.storage_handler import StorageHandler

app = Flask(__name__)
logins = LoginHandler.instance()
s3 = StorageHandler()

def is_request_valid(request):
    is_token_valid = request.form['token'] == logins.getSlackToken()
    is_team_id_valid = request.form['team_id'] == logins.getTeamId()

    return is_token_valid and is_team_id_valid


@app.route('/letmein', methods=['POST'])
def openTheDoor():
    if not is_request_valid(request):
        abort(400)

    return jsonify(
        response_type='in_channel',
        text='<https://youtu.be/frszEJb0aOo|General Kenobi!>',
    )