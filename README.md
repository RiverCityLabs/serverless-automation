# RCL Serverless-Automation

This project aims to automate manual tasks of running the makerspace using serverless technologies

# Local development setup
## Install Python
This project uses the latest stable python 3.7. Please install it via their [website](https://www.python.org/downloads/)

Python comes with `pip` preinstalled, we'll be using it later.

Next, you may need to add the python/Scripts install directory to your system PATH variable to be able to use `pip` and `pipenv`. That's a challenge left for the reader.

## Install Pipenv

`Pipenv` combines the `pip` package manager and the `virtualenv` module that creates virtual python environments. With pipenv, we can declare separate dependencies for every virtual python environment, even different python versions! For now though, we're using it for reproducible builds.

Run this in a command prompt to install pipenv:
    
    pip install pipenv

Next we'll set an environment variable that will tell pipenv to install any virtual environments in the folder it's called in, instead of a system folder. This will need to be done every time you restart your terminal session.

linux:

    export PIPENV_VENV_IN_PROJECT=1

windows (cmd):

    set PIPENV_VENV_IN_PROJECT=1

Now that we have everything set, we can create our first python virtual environment with pipenv:

```cd``` into a function's folder and run:

    pipenv install

This will cause pipenv to read the pip.lock file and install all dependencies for the environment.

## .env file setup
By default, pipenv will load any .env file and add any environment variables it finds within. This is great for reading in passwords because we can block .env files from being commited to the repo.

Create a file **inside** a function folder named ``.env`` and copy the below variables inside. You will need to find the actual passwords elsewhere and replace them here:
    
    LOCAL-DEVELOPMENT=TRUE
    GPW_PW= gpwpasswordhere
    AWS_KEY = awskeyhere
    AWS_SECRET = awssecrethere
    SLACK_API_TOKEN = slaacktokenhere
    SLACK_LEGACY_TOKEN = slacklegacytokenhere
    MAILGUN_API_KEY = mailgunapikeyhere



# Running functions locally

Now that this stack is in AWS lambda functions, you need to do a little hacking to get them to run locally (until I figure out and document how to invoke lambdas locally)

You will need to scroll to the bottom of the file and uncomment the line that calls the handler() function. Then you can run the script normally.

To run a python script, ```cd``` into the functions directory and run:

    pipenv run python slack_audit.py